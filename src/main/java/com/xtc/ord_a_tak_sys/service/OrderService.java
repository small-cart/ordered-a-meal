package com.xtc.ord_a_tak_sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xtc.ord_a_tak_sys.entity.Orders;

public interface OrderService extends IService<Orders> {

    /**
     * 用户下单
     * @param orders
     */
    public void submit(Orders orders);
}
