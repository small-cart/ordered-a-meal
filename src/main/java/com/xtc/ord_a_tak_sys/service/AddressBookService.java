package com.xtc.ord_a_tak_sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xtc.ord_a_tak_sys.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {

}
