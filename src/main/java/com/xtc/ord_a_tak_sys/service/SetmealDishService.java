package com.xtc.ord_a_tak_sys.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.xtc.ord_a_tak_sys.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
