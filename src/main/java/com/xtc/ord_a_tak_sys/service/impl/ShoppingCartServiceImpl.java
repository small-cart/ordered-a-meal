package com.xtc.ord_a_tak_sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xtc.ord_a_tak_sys.entity.ShoppingCart;
import com.xtc.ord_a_tak_sys.mapper.ShoppingCartMapper;
import com.xtc.ord_a_tak_sys.service.ShoppingCartService;
import org.springframework.stereotype.Service;

@Service
public class ShoppingCartServiceImpl extends ServiceImpl<ShoppingCartMapper, ShoppingCart> implements ShoppingCartService {

}
