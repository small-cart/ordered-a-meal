package com.xtc.ord_a_tak_sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xtc.ord_a_tak_sys.entity.SetmealDish;
import com.xtc.ord_a_tak_sys.mapper.SetmealDishMapper;
import com.xtc.ord_a_tak_sys.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SetmealDishServiceImpl extends ServiceImpl<SetmealDishMapper,SetmealDish> implements SetmealDishService {
}
