package com.xtc.ord_a_tak_sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xtc.ord_a_tak_sys.entity.User;
import com.xtc.ord_a_tak_sys.mapper.UserMapper;
import com.xtc.ord_a_tak_sys.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper,User> implements UserService{
}
