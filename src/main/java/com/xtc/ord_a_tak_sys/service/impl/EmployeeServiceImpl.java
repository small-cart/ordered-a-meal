package com.xtc.ord_a_tak_sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xtc.ord_a_tak_sys.entity.Employee;
import com.xtc.ord_a_tak_sys.mapper.EmployeeMapper;
import com.xtc.ord_a_tak_sys.service.EmployeeService;
import org.springframework.stereotype.Service;

@Service
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper,Employee> implements EmployeeService{
}
