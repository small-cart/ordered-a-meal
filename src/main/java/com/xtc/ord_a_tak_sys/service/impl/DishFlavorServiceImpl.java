package com.xtc.ord_a_tak_sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xtc.ord_a_tak_sys.entity.DishFlavor;
import com.xtc.ord_a_tak_sys.mapper.DishFlavorMapper;
import com.xtc.ord_a_tak_sys.service.DishFlavorService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper,DishFlavor> implements DishFlavorService {
}
