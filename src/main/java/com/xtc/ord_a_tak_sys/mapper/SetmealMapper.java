package com.xtc.ord_a_tak_sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xtc.ord_a_tak_sys.entity.Setmeal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealMapper extends BaseMapper<Setmeal> {
}
