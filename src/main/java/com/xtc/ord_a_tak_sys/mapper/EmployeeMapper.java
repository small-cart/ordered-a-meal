package com.xtc.ord_a_tak_sys.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xtc.ord_a_tak_sys.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmployeeMapper extends BaseMapper<Employee>{
}
