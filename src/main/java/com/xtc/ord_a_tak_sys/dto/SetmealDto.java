package com.xtc.ord_a_tak_sys.dto;

import com.xtc.ord_a_tak_sys.entity.Setmeal;
import com.xtc.ord_a_tak_sys.entity.SetmealDish;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
