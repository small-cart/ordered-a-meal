package com.xtc.ord_a_tak_sys.dto;

import com.xtc.ord_a_tak_sys.entity.Dish;
import com.xtc.ord_a_tak_sys.entity.DishFlavor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto extends Dish {

    //菜品对应的口味数据
    private List<DishFlavor> flavors = new ArrayList<>();

    private String categoryName;

    private Integer copies;
}
