package com.xtc.ord_a_tak_sys.controller;

import com.xtc.ord_a_tak_sys.common.R;
import com.xtc.ord_a_tak_sys.entity.OrderDetail;
import com.xtc.ord_a_tak_sys.service.OrderDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单明细
 */
@Slf4j
@RestController
@RequestMapping("/orderDetail")
public class OrderDetailController {

    @Autowired
    private OrderDetailService orderDetailService;
    @GetMapping("/{id}")
    public R getOrderDetailById(@PathVariable Integer id){
        OrderDetail byId = orderDetailService.getById(id);
        return R.success(byId);
    }

}