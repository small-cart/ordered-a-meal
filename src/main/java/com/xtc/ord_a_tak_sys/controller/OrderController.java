package com.xtc.ord_a_tak_sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xtc.ord_a_tak_sys.common.R;
import com.xtc.ord_a_tak_sys.entity.Orders;
import com.xtc.ord_a_tak_sys.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 用户下单
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders){
        log.info("订单数据：{}",orders);
        orderService.submit(orders);
        return R.success("下单成功");
    }
    /**
     * 菜品信息分页查询
     * @param page
     * @param pageSize
     * @param number
     * @return
     */
    @GetMapping("/page")
    public R Page(int page,int pageSize,String number){
        Page<Orders> pages = new Page<>(page,pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();
        // 订单号模糊查询
        queryWrapper.like(number != null,Orders::getNumber,number);
        // 按订单号排序
        queryWrapper.orderByDesc(Orders::getNumber);
        Page<Orders> page1 = orderService.page(pages, queryWrapper);
        return R.success(page1);
    }

    @GetMapping("/userPage")
    public R userPage(int page,int pageSize){
        Page<Orders> pages = new Page<>(page,pageSize);

        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();
        // 按订单号排序
        queryWrapper.orderByDesc(Orders::getNumber);
        Page<Orders> page1 = orderService.page(pages, queryWrapper);
        return R.success(page1);
    }
}